<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>DetectorTableModel</name>
    <message>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>활성화</translation>
    </message>
</context>
<context>
    <name>OpenRGBClientInfoPageUi</name>
    <message>
        <source>Client info page</source>
        <translation>클라이언트 정보 페이지</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>연결</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Connected Clients</source>
        <translation>연결된 클라이언트</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>프로토콜 버전</translation>
    </message>
    <message>
        <source>Save Connection</source>
        <translation>연결 저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBConsolePageUi</name>
    <message>
        <source>Log console page</source>
        <translation>로그 콘솔 페이지</translation>
    </message>
    <message>
        <source>Log level</source>
        <translation>로그 수준</translation>
    </message>
    <message>
        <source>Refresh logs</source>
        <translation type="unfinished">로그 새로고침</translation>
    </message>
    <message>
        <source>Clear log</source>
        <translation type="unfinished">로그</translation>
    </message>
</context>
<context>
    <name>OpenRGBDeviceInfoPageUi</name>
    <message>
        <source>Device info page</source>
        <translation>장치 정보 페이지</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>Vendor:</source>
        <translation>제조업체:</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>유형:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>설명:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>버전</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation>위치</translation>
    </message>
    <message>
        <source>Serial:</source>
        <translation>시리얼:</translation>
    </message>
</context>
<context>
    <name>OpenRGBDevicePageUi</name>
    <message>
        <source>Device page</source>
        <translation>장치 페이지</translation>
    </message>
    <message>
        <source>G:</source>
        <translation type="unfinished">초록(G):</translation>
    </message>
    <message>
        <source>H:</source>
        <translation type="unfinished">색상(H):</translation>
    </message>
    <message>
        <source>Speed:</source>
        <translation>속도:</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>랜덤:</translation>
    </message>
    <message>
        <source>B:</source>
        <translation type="unfinished">파랑(B):</translation>
    </message>
    <message>
        <source>LED:</source>
        <translation>LED:</translation>
    </message>
    <message>
        <source>Mode-Specific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>R:</source>
        <translation type="unfinished">빨강(R):</translation>
    </message>
    <message>
        <source>Dir:</source>
        <translation type="unfinished">위치:</translation>
    </message>
    <message>
        <source>S:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="unfinished">모두 선택</translation>
    </message>
    <message>
        <source>Per-LED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zone:</source>
        <translation>구역:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Sets all devices to&lt;br/&gt;&lt;b&gt;Static&lt;/b&gt; mode and&lt;br/&gt;applies the selected color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Sets all devices to&lt;br/&gt;&lt;b&gt;Static&lt;/b&gt; mode and&lt;br/&gt;applies the selected color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Apply All Devices</source>
        <translation>모든 기기에 적용</translation>
    </message>
    <message>
        <source>Colors:</source>
        <translation>색상:</translation>
    </message>
    <message>
        <source>V:</source>
        <translation>명도(V):</translation>
    </message>
    <message>
        <source>Apply Colors To Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mode:</source>
        <translation type="unfinished">모드:</translation>
    </message>
    <message>
        <source>Brightness:</source>
        <translation type="unfinished">밝기:</translation>
    </message>
    <message>
        <source>Save To Device</source>
        <translation>장치에 저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBDialog2Ui</name>
    <message>
        <source>OpenRGB</source>
        <translation>OpenRGB (한국어화: OctopusET)</translation>
    </message>
    <message>
        <source>Devices</source>
        <translation>장치</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>정보</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>설정</translation>
    </message>
    <message>
        <source>Toggle LED View</source>
        <translation type="unfinished">LED 보기 토글</translation>
    </message>
    <message>
        <source>Rescan Devices</source>
        <translation type="unfinished">장치 재검사</translation>
    </message>
    <message>
        <source>Save Profile</source>
        <translation>프로파일 저장</translation>
    </message>
    <message>
        <source>Delete Profile</source>
        <translation>프로파일 삭제</translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation>프로파일 불러오기</translation>
    </message>
    <message>
        <source>OpenRGB is detecting devices...</source>
        <translation>OpenRGB가 장치를 찾고 있습니다...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <source>Save Profile As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Profile with custom name</source>
        <translation>다른 이름으로 프로파일 저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBDialogUi</name>
    <message>
        <source>OpenRGB</source>
        <translation>OpenRGB</translation>
    </message>
    <message>
        <source>Device</source>
        <translation>장치</translation>
    </message>
    <message>
        <source>Red</source>
        <translation>빨강</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>초록</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>파랑</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation>모드</translation>
    </message>
    <message>
        <source>Set All</source>
        <translation>모두 설정</translation>
    </message>
    <message>
        <source>Set Device</source>
        <translation>장치 설정</translation>
    </message>
    <message>
        <source>Zone</source>
        <translation>구역</translation>
    </message>
    <message>
        <source>Set Zone</source>
        <translation>구역 설정</translation>
    </message>
    <message>
        <source>Set LED</source>
        <translation>LED 설정</translation>
    </message>
    <message>
        <source>LED</source>
        <translation>LED</translation>
    </message>
</context>
<context>
    <name>OpenRGBE131SettingsEntryUi</name>
    <message>
        <source>E131 settings entry</source>
        <translation type="unfinished">E131 설정 </translation>
    </message>
    <message>
        <source>Start Channel:</source>
        <translation type="unfinished">채널 시작:</translation>
    </message>
    <message>
        <source>Number of LEDs:</source>
        <translation>LED 개수:</translation>
    </message>
    <message>
        <source>Start Universe:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>Matrix Order:</source>
        <translation type="unfinished">매트릭스 순서:</translation>
    </message>
    <message>
        <source>Matrix Height:</source>
        <translation type="unfinished">매트릭스 높이</translation>
    </message>
    <message>
        <source>Matrix Width:</source>
        <translation>매트릭스 너비:</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>유형:</translation>
    </message>
    <message>
        <source>IP (Unicast):</source>
        <translation type="unfinished">IP (Unicast)</translation>
    </message>
    <message>
        <source>Universe Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keepalive Time:</source>
        <translation>지속 시간:</translation>
    </message>
    <message>
        <source>RGB Order:</source>
        <translation>RGB 순서:</translation>
    </message>
</context>
<context>
    <name>OpenRGBE131SettingsPageUi</name>
    <message>
        <source>E131 settings page</source>
        <translation>E131 설정 페이지</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBElgatoKeyLightSettingsEntryUi</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
</context>
<context>
    <name>OpenRGBElgatoKeyLightSettingsPageUi</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBLIFXSettingsEntryUi</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>이름:</translation>
    </message>
</context>
<context>
    <name>OpenRGBLIFXSettingsPageUi</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBNanoleafSettingsEntryUi</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <source>Auth Key:</source>
        <translation>인증 키:</translation>
    </message>
    <message>
        <source>Unpair</source>
        <translation>페어링 해제</translation>
    </message>
    <message>
        <source>Pair</source>
        <translation>페어링</translation>
    </message>
</context>
<context>
    <name>OpenRGBNanoleafSettingsPageUi</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan</source>
        <translation>스캔</translation>
    </message>
    <message>
        <source>To pair, hold the on-off button down for 5-7 seconds until the LED starts flashing in a pattern, then click the &quot;Pair&quot; button within 30 seconds.</source>
        <translation>페어링을 하려면 온오프 버튼을 5-7초 LED가 규칙적으로 깜빡이기 시작할 때 까지 누르세요, 그리고 30초 내로 &quot;페어링&quot; 버튼을 누르세요.</translation>
    </message>
</context>
<context>
    <name>OpenRGBPhilipsHueSettingsEntryUi</name>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <source>Entertainment Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Username:</translation>
    </message>
    <message>
        <source>Client Key:</source>
        <translation>클라이언트 키:</translation>
    </message>
    <message>
        <source>Unpair Bridge</source>
        <translation>브릿지 페어링 해제:</translation>
    </message>
    <message>
        <source>MAC:</source>
        <translation>MAC 주소:</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Connect Group:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBPhilipsHueSettingsPageUi</name>
    <message>
        <source>Philips Hue settings page</source>
        <translation type="unfinished">필립스 Hue 설정</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <source>After adding a Hue entry and saving, restart OpenRGB and press the Sync button on your Hue bridge to pair it.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBPhilipsWizSettingsEntryUi</name>
    <message>
        <source>Philips WIZ settings entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
</context>
<context>
    <name>OpenRGBPhilipsWizSettingsPageUi</name>
    <message>
        <source>Philips WIZ settings page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBPluginContainerUi</name>
    <message>
        <source>Plugin container</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBPluginsEntryUi</name>
    <message>
        <source>Plugins entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>버전:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>설명:</translation>
    </message>
    <message>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation>경로:</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>활성화</translation>
    </message>
    <message>
        <source>Commit:</source>
        <translation>커밋:</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation>아이콘</translation>
    </message>
</context>
<context>
    <name>OpenRGBPluginsPageUi</name>
    <message>
        <source>Plugins page</source>
        <translation>플러그인 페이지</translation>
    </message>
    <message>
        <source>Install Plugin</source>
        <translation>플러그인 설치</translation>
    </message>
    <message>
        <source>Remove Plugin</source>
        <translation>플러그인 제거</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Looking for plugins? See the official list at &lt;a href=&quot;https://openrgb.org/plugins.html&quot;&gt;OpenRGB.org&lt;/a&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;플러그인을 찾고 있나요? &lt;a href=&quot;https://openrgb.org/plugins.html&quot;&gt;OpenRGB.org&lt;/a&gt;&lt;/body&gt;&lt;/html&gt;의 공식 목록을 참고하세요.</translation>
    </message>
</context>
<context>
    <name>OpenRGBProfileSaveDialogUi</name>
    <message>
        <source>Profile Name</source>
        <translation>프로파일 이름</translation>
    </message>
    <message>
        <source>New Profile Name:</source>
        <translation>새 프로파일 이름:</translation>
    </message>
</context>
<context>
    <name>OpenRGBQMKORGBSettingsEntryUi</name>
    <message>
        <source>QMK settings entry</source>
        <translation type="unfinished">QMK 설정 </translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>USB PID:</source>
        <translation>USB PID:</translation>
    </message>
    <message>
        <source>USB VID:</source>
        <translation>USB VID:</translation>
    </message>
</context>
<context>
    <name>OpenRGBQMKORGBSettingsPageUi</name>
    <message>
        <source>QMK Settings page</source>
        <translation>QMK 설정 페이지</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBSerialSettingsEntryUi</name>
    <message>
        <source>Serial settings entry</source>
        <translation type="unfinished">시리얼 설정</translation>
    </message>
    <message>
        <source>Baud:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <source>Number of LEDs:</source>
        <translation>LED 개수:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation>프로토콜:</translation>
    </message>
</context>
<context>
    <name>OpenRGBSerialSettingsPageUi</name>
    <message>
        <source>Serial settings page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBServerInfoPageUi</name>
    <message>
        <source>Server info page</source>
        <translation>서버 정보 페이지</translation>
    </message>
    <message>
        <source>Stop Server</source>
        <translation>서버 종료</translation>
    </message>
    <message>
        <source>Server Port:</source>
        <translation>서버 포트:</translation>
    </message>
    <message>
        <source>Start Server</source>
        <translation>서버 시작</translation>
    </message>
    <message>
        <source>Server Status:</source>
        <translation>서버 상태:</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>오프라인</translation>
    </message>
    <message>
        <source>Connected Clients:</source>
        <translation>연결된 클라이언트:</translation>
    </message>
    <message>
        <source>Server Host:</source>
        <translation>서버 호스트:</translation>
    </message>
    <message>
        <source>Client IP</source>
        <translation>클라이언트 IP</translation>
    </message>
    <message>
        <source>Protocol Version</source>
        <translation>프로토콜 버전</translation>
    </message>
    <message>
        <source>Client Name</source>
        <translation>클라이언트 이름</translation>
    </message>
</context>
<context>
    <name>OpenRGBSettingsPageUi</name>
    <message>
        <source>Settings page</source>
        <translation>설정 페이지</translation>
    </message>
    <message>
        <source>Load Window Geometry</source>
        <translation type="unfinished">윈도우 지오메트리 불러오기</translation>
    </message>
    <message>
        <source>90000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run zone checks on rescan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start Server</source>
        <translation>서버 시작</translation>
    </message>
    <message>
        <source>Start Minimized</source>
        <translation type="unfinished">최소화 상태로 시작</translation>
    </message>
    <message>
        <source>User Interface Settings:</source>
        <translation>UI 설정:</translation>
    </message>
    <message>
        <source>Start At Login</source>
        <translation type="unfinished">로그인시에 시작</translation>
    </message>
    <message>
        <source>Minimize On Close</source>
        <translation type="unfinished">닫지 않고 최소화 하기</translation>
    </message>
    <message>
        <source>Save Geometry On Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start Client</source>
        <translation>클라이언트 시작</translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation>프로파일 불러오기</translation>
    </message>
    <message>
        <source>Set Server Port</source>
        <translation>서버 포트 설정</translation>
    </message>
    <message>
        <source>Theme (restart required)</source>
        <translation>테마 (재시작 필요)</translation>
    </message>
    <message>
        <source>Enable Log Console (restart required)</source>
        <translation>콘솔 로그 활성화 (재시작 필요)</translation>
    </message>
    <message>
        <source>Custom Arguments</source>
        <translation type="unfinished">사용자 인자</translation>
    </message>
    <message>
        <source>Log Manager Settings:</source>
        <translation>로그 관리자 설정</translation>
    </message>
    <message>
        <source>Start at Login Status</source>
        <translation type="unfinished">로그인시에 시작 상태</translation>
    </message>
    <message>
        <source>Start At Login Settings:</source>
        <translation>로그인시에 시작 설정:</translation>
    </message>
    <message>
        <source>Open Settings Folder</source>
        <translation>설정 폴더 열기</translation>
    </message>
    <message>
        <source>Drivers Settings</source>
        <translation>드라이버 설정</translation>
    </message>
    <message>
        <source>Greyscale Tray Icon</source>
        <translation type="unfinished">흑백 트레이 아이콘</translation>
    </message>
    <message>
        <source>AMD SMBus: Reduce CPU Usage (restart required)</source>
        <translation>AMD SMBus : CPU 사용량 감소 (재시작 필요)</translation>
    </message>
    <message>
        <source>Set Profile on Exit</source>
        <translation type="unfinished">종료시 프로파일 설정</translation>
    </message>
    <message>
        <source>Shared SMBus Access (restart required)</source>
        <translation type="unfinished">공유된 SMBus 접근 (재시작 필요)</translation>
    </message>
    <message>
        <source>Set Server Host</source>
        <translation>서버 호스트 설정</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>언어 (Language)</translation>
    </message>
</context>
<context>
    <name>OpenRGBSoftwareInfoPageUi</name>
    <message>
        <source>Software info page</source>
        <translation>소프트웨어 정보 페이지</translation>
    </message>
    <message>
        <source>Build Date:</source>
        <translation>빌드 날짜:</translation>
    </message>
    <message>
        <source>Git Commit ID:</source>
        <translation>Git 커밋 ID:</translation>
    </message>
    <message>
        <source>Git Commit Date:</source>
        <translation>Git 커밋 날짜:</translation>
    </message>
    <message>
        <source>Git Branch:</source>
        <translation>Git 브랜치:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>버전:</translation>
    </message>
    <message>
        <source>Gitlab page</source>
        <translation>Gitlab 페이지</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>웹사이트</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;https://openrgb.org&quot;&gt;https://openrgb.org&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://openrgb.org&quot;&gt;https://openrgb.org&lt;/a&gt;</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;https://gitlab.com/CalcProgrammer1/OpenRGB&quot;&gt;https://gitlab.com/CalcProgrammer1/OpenRGB&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://gitlab.com/CalcProgrammer1/OpenRGB&quot;&gt;https://gitlab.com/CalcProgrammer1/OpenRGB&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>OpenRGBSupportedDevicesPageUi</name>
    <message>
        <source>Supported devices</source>
        <translation>지원 기기</translation>
    </message>
    <message>
        <source>Filter:</source>
        <translation>필터</translation>
    </message>
    <message>
        <source>Enable/Disable all</source>
        <translation>전부 활성화/비활성화</translation>
    </message>
    <message>
        <source>Apply changes</source>
        <translation type="unfinished">수정사항 적용</translation>
    </message>
</context>
<context>
    <name>OpenRGBSystemInfoPageUi</name>
    <message>
        <source>System info page</source>
        <translation>시스템 정보 페이지</translation>
    </message>
    <message>
        <source>SMBus Adapters:</source>
        <translation>SMBus 어댑터:</translation>
    </message>
    <message>
        <source>Address:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <source>Read Device</source>
        <translation>장치 읽기:</translation>
    </message>
    <message>
        <source>SMBus Dumper:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0x</source>
        <translation>0x</translation>
    </message>
    <message>
        <source>SMBus Detector:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Detection Mode:</source>
        <translation>감지 모드:</translation>
    </message>
    <message>
        <source>Detect Devices</source>
        <translation>장치 감지</translation>
    </message>
    <message>
        <source>Dump Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SMBus Reader:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Addr:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <source>Reg:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="unfinished">용량:</translation>
    </message>
</context>
<context>
    <name>OpenRGBYeelightSettingsEntryUi</name>
    <message>
        <source>Yeelight settings entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP:</source>
        <translation>IP 주소:</translation>
    </message>
    <message>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <source>Music Mode:</source>
        <translation>음악 모드:</translation>
    </message>
    <message>
        <source>Override host IP:</source>
        <translation type="unfinished">호스트 IP 덮어쓰기:</translation>
    </message>
    <message>
        <source>Left blank for auto discovering host ip</source>
        <translation type="unfinished">비워두면 자동으로 호스트 IP 감지</translation>
    </message>
</context>
<context>
    <name>OpenRGBYeelightSettingsPageUi</name>
    <message>
        <source>Yeelight settings page</source>
        <translation>Yeelight 설정 창</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
</context>
<context>
    <name>OpenRGBZoneResizeDialogUi</name>
    <message>
        <source>Resize Zone</source>
        <translation>구역 재설정</translation>
    </message>
</context>
<context>
    <name>OpenRGBZonesBulkResizerUi</name>
    <message>
        <source>Zones resizer</source>
        <translation>구역 재설정 도구</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;One or more resizable zones have not been configured.  Resizable zones are most commonly used for addressable RGB headers where the size of the connected device cannot be detected automatically.&lt;/p&gt;&lt;p&gt;Please enter the number of LEDs in each zone below.&lt;/p&gt;&lt;p&gt;For more information about calcuating the correct size, please check &lt;a href=&quot;https://openrgb.org/resize&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;this link.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show again</source>
        <translation type="unfinished">다시 보이지 않기</translation>
    </message>
    <message>
        <source>Save and close</source>
        <translation>저장하고 닫기</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>무시</translation>
    </message>
</context>
<context>
    <name>TabLabelUi</name>
    <message>
        <source>Tab Label</source>
        <translation type="unfinished">탭 라벨</translation>
    </message>
    <message>
        <source>device name</source>
        <translation>장치 이름</translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBClientInfoPage</name>
    <message>
        <source>Disconnect</source>
        <translation>연결 끊기</translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBDevicePage</name>
    <message>
        <source>Set individual LEDs to static colors.  Safe for use with software-driven effects.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set individual LEDs to static colors.  Not safe for use with software-driven effects.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sets the entire device or a zone to a single color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gradually fades between fully off and fully on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abruptly changes between fully off and fully on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gradually cycles through the entire color spectrum.  All lights on the device are the same color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gradually cycles through the entire color spectrum.  Produces a rainbow pattern that moves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flashes lights when keys or buttons are pressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Entire Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Entire Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved To Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save To Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saving Not Supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mode Specific</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBDialog2</name>
    <message>
        <source>Show/Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quick Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lights Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E1.31 Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Philips Hue Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Philips Wiz Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OpenRGB QMK Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Serial Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yeelight Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SMBus Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDK Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDK Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you really want to delete this profile?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Log Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LIFX Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nanoleaf Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elgato KeyLight Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Supported Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBE131SettingsEntry</name>
    <message>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Matrix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Top Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Top Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Bottom Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Bottom Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical Top Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical Top Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical Bottom Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical Bottom Right</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBPluginsPage</name>
    <message>
        <source>Install OpenRGB Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plugin files (*.dll *.dylib *.so *.so.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replace Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A plugin with this filename is already installed.  Are you sure you want to replace this plugin?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to remove this plugin?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBServerInfoPage</name>
    <message>
        <source>Stopping...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBSettingsPage</name>
    <message>
        <source>A problem occurred enabling Start At Login.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>English - US</source>
        <translation type="unfinished">English - US</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBYeelightSettingsEntry</name>
    <message>
        <source>Choose an IP...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose the correct IP for the host</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ui::OpenRGBZonesBulkResizer</name>
    <message>
        <source>Resize the zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
